<?php

namespace App\Http\Controllers;

use App\HomePost;
use Illuminate\Http\Request;

class HomePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * insert point for logged user
     * wiht his name he insert lat, long and the action data
     * then save data
     */
    public function insertAction(Request $request)
    {
        $data = $request->all();
        $item = HomePost::create($data)->id;
        if ($item != null) {
            return response()->json([
                "status" => true,
                "msg" => "Post Saved ",
                'Post_Id' => $item,
                "message" => array([
                'Post_Id' => $item,
                ])

            ]);
        } else {
            return response()->json([
                "status" => false,
                "msg" => "Error in Saved item",
                "message" => array([

                ])]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * get all  point that user insert
     * all user data will be shown for public people without name
     */
    public function getAllAction()
    {
        $Posts = HomePost::where('flag', '1')->with('image')->get();
        //->paginate(10);
        if (count($Posts) != 0) {
            return response()->json([
                "status" => true,
                "code" => 200,
                "messages" => "user Public Points ",
                "result" => $Posts
            ]);
        } else {
            return response()->json([
                "status" => false,
                "code" => 401,
                "messages" => "No posts Found",
                "result" => []
            ]);

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * get all data that user insert before
     */
    public function getUserPoints(Request $request)
    {
        $apiToken = $request->header('Authorization');
        $token = str_replace("Bearer ", "", $apiToken);

        $user = HomePost::whereuser_token($token)->get();
        if (count($user) != 0)
            //return response()->json(['success' => $user]);
            return response()->json([
                "status" => true,
                "code" => 200,
                "messages" => "user Points ",
                "result" => $user
            ]);
        else {
            return response()->json([
                "status" => false,
                "code" => 401,
                "message" => "No Points Found",
                "result" => []
            ]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function updateOld(Request $request, $id)
    {
        //dd($request->get('event_content'));
        $request->validate([
            'event_content' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'img_id' => 'required',
            'event_content' => 'required',

        ]);
        $contact = HomePost::find($id);
        if (count($contact) != 0) {
            $contact->event_content = $request->get('event_content');
            $contact->latitude = $request->get('latitude');
            $contact->longitude = $request->get('longitude');
            $contact->user_token = $request->get('user_token');
            $contact->img_id = $request->get('img_id');
            $contact->flag = $request->get('flag');

            if ($contact->save()) {


                return response()->json([
                    "status" => true,
                    "msg" => "Points update",

                ]);
            } else {
                return response()->json([
                    "status" => false,
                    "msg" => "Points failed to update ",

                ]);
            }
        } else {
            return response()->json([
                "status" => false,
                "msg" => "No Points found to save ",

            ]);
        }
    }
    public function update(Request $request, $id)
    {

        //dd($request->get('event_content'));
//        $request->validate([
//            'event_content' => 'required',
//            'latitude' => 'required',
//            'longitude' => 'required',
//            'img_id' => 'required',
//            'event_content' => 'required',
//
//        ]);
        $contact = HomePost::find($id);
        if (count($contact) != 0) {
            $contact->event_content = $request->get('event_content');
           /* $contact->latitude = $request->get('latitude');
            $contact->longitude = $request->get('longitude');
            $contact->user_token = $request->get('user_token');
            $contact->img_id = $request->get('img_id');
            $contact->flag = $request->get('flag');*/

            if ($contact->save()) {


                return response()->json([
                    "status" => true,
                    "msg" => "Points update",

                ]);
            } else {
                return response()->json([
                    "status" => false,
                    "msg" => "Points failed to update ",

                ]);
            }
        } else {
            return response()->json([
                "status" => false,
                "msg" => "No Points found to save ",

            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = HomePost::find($id);
        if (count($contact) != 0) {
            $contact->delete();
            return response()->json([
                "status" => true,
                "msg" => "Points delete",

            ]);
        } else {
            return response()->json([
                "status" => true,
                "msg" => "Points not found",

            ]);
        }
    }

    public function archiveItem($id)
    {
        $page = HomePost::find($id);

// Make sure you've got the Page model
        if ($page) {
            $page->flag = '0';
            $page->save();
        }
    }
}
