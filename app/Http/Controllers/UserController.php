<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            // $success['token'] = $user;
            return response()->json(['successCode' => 200, 'statues' => true, 'message' => "UserLogin success", "user" => $user]);
        } else {
            return response()->json(['successCode' => 401, 'statues' => false, 'message' => "UserLogin failed", "user" => null]);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        /**
         * Check for Exist mail
         * if Found --> return to user mail is register before
         * if NotFound --> return to user registration Process
         */
        if ($this->checkExistMail($request->email)) {
            return response()->json(['successCode' => 401,
                'statues' => false,
                'message' => "This mail is register before",
                "user" => null]);

        }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }


        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['api_token'] = bcrypt($request->email);

        $user = User::create($input);
        if ($user != null) {
            return response()->json(['successCode' => 200, 'statues' => true, 'message' => "UserLogin success", "user" => $user]);
        } else {
            return response()->json(['successCode' => 401, 'statues' => false, 'message' => "UserLogin failed", "user" => null]);

        }

    }

    function checkExistMail($userMail)
    {
        $category = User::where('email', $userMail)->exists();
        return $category;
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request)
    {
        $apiToken = $request->header('Authorization');
        $token = str_replace("Bearer ", "", $apiToken);

        $user = User::whereApiToken($token)->first();

        return response()->json(['success' => $user], $this->successStatus);
    }

}