<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    public function Post()
    {
        return $this->hasMany('App\HomePost', 'img_id');
    }

    public static function getPathAttribute($value)
    {
        return asset('/imagePoint/'.$value);
    }
}
