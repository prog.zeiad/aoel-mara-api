<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePost extends Model
{
    //
    protected $fillable = ['event_content', 'latitude', 'longitude', 'user_token', 'img_id', 'flag'];

    public function image()
    {
        return $this->belongsTo('App\Picture', 'img_id');
    }
}
