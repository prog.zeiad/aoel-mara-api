<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::get('details', 'UserController@details');
Route::get('getAllPoint', 'HomePostController@getAllAction');
Route::get('getUserPoints', 'HomePostController@getUserPoints');
Route::post('/insertAction', 'HomePostController@insertAction');
Route::put('/editUserPoint/{id}', 'HomePostController@update');
Route::delete('/deletePoint/{id}', 'HomePostController@destroy');
Route::put('/archiveItem/{id}', 'HomePostController@archiveItem');

//---get all image available
Route::get('getApprovedImage', 'PictureController@getApprovedImage');

Route::group(['middleware' => 'auth:api'], function () {

});