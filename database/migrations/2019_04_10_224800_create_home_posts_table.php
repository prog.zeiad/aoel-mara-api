<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('event_content');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('user_token');
            $table->string('image_path');
            $table->boolean('flag');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_posts');
    }
}
